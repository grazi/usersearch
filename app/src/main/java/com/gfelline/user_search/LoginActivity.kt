package com.gfelline.user_search

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }


    fun loginBtnClicked(view:View) {

        val username = loginUsernameTxt.text.toString()
        if (username == "admin"){
            val password = loginPasswordTxt.text.toString()
            if (password == "secret") {
                val goToModeActivityIntent = Intent(this, ModeActivity::class.java)
                startActivity(goToModeActivityIntent)
            } else {
                Toast.makeText(this, "Kein Zugriff, falsches Passwort", Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(this, "Kein Zugriff, kenne $username nicht", Toast.LENGTH_LONG).show()
        }

    }

}
