package com.gfelline.user_search

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.gfelline.user_search.model.Database
import com.gfelline.user_search.model.User
import kotlinx.android.synthetic.main.activity_regist_user.*

class RegistUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regist_user)
    }


    fun registUserClicked(view:View) {

        val newUser = User(
                registUsernameTxt.text.toString(),
                registPasswordTxt.text.toString(),
                registAdressTxt.text.toString(),
                registLocationTxt.text.toString(),
                registPhoneNrTxt.text.toString())

        Database.addUser(newUser)

        Toast.makeText(this, "User ${newUser.username} hinzugefügt", Toast.LENGTH_LONG).show()

        clearFields()

    }

    fun clearFields() {
        registUsernameTxt.text.clear()
        registPasswordTxt.text.clear()
        registAdressTxt.text.clear()
        registLocationTxt.text.clear()
        registPhoneNrTxt.text.clear()

    }
}
