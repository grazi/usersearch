package com.gfelline.user_search

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.gfelline.user_search.model.Database
import kotlinx.android.synthetic.main.activity_view_user.*

class ViewUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_user)
    }

    fun searchBtnClicked(view:View) {
        val searchText = searchTxt.text.toString()

        for (einUser in Database.findAllUsers()) {
            if (einUser.username == searchText) {
                viewUsernameTxt.setText(einUser.username)
                viewPasswordTxt.setText(einUser.password)
                viewAdressTxt.setText(einUser.adress)
                viewLocationTxt.setText(einUser.location)
                viewPhoneNrTxt.setText(einUser.phoneNr)
            }
        }
    }
}