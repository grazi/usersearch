package com.gfelline.user_search.model

// Singleton
object Database {

    val users = arrayListOf<User>()

    fun addUser(user:User) {
        users.add(user)
    }

    fun findAllUsers(): List<User> {
        return users
    }

}