package com.gfelline.user_search.model

/**
 * Created by gfelline on 11/20/17.
 */
data class User(val username:String,
           val password:String,
           val adress:String,
           val location:String,
           val phoneNr:String) {

}