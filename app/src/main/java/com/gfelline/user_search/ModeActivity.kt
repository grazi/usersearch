package com.gfelline.user_search

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class ModeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mode)
    }

    fun registBtnClicked(view:View) {
        val goToRegistUserActivityIntent = Intent(this, RegistUserActivity::class.java)
        startActivity(goToRegistUserActivityIntent)
    }

    fun userBtnClicked(view: View) {
        val goToViewUserActivityIntent = Intent(this, ViewUserActivity::class.java)
        startActivity(goToViewUserActivityIntent)
    }

}
